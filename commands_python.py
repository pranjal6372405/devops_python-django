# python3 --version
# python3 --version

# touch command_python.py
# vim command_python.py
# python3 vommand_python.py
# python --version

# Strings and Number Data Types

print("20 days are" + str(50) + " minutes")
print("20 days are {50} minutes")

#Variables

print(f"20 days are {20*24*60} minutes")
print(f"20 days are {20*24*60} minutes")

calculation_to_units = 24 * 60 * 60
print(f"20 days are {20 * calculation_to_units} seconds")
print(f"35 days are {35 * calculation_to_units} seconds")
print(f"50 days are {50 * calculation_to_units} seconds")
print(f"110 days are {110 * calculation_to_units} seconds")

calculation_to_hours = 24
name_of_unit = "hours"

def days_to_units(num_of_days):
    print(f"{num_of_days} days are {num_of_days * calculation_to_hours} {name_of_unit}")

days_to_units(20)
days_to_units(35)
days_to_units(50)
days_to_units(110)

#Scope

def days_to_units(num_of_days, custom_message):
    print(f"{num_of_days} days are {num_of_days * calculation_to_hours} {name_of_unit}")
    print(custom_message)

def scope_check():
    print(name_of_unit)

# Encapsulate, Logic with Functions
    
calculation_to_units = 24
name_of_unit = "hours"

def days_to_units(num_of_days):
    print(f"{num_of_days} days are {num_of_days * calculation_to_units} {name_of_unit}")
    print("All good!")

days_to_units(35)

days_to_units(20)
days_to_units(35)
days_to_units(50)
days_to_units(110)

# scope
calculation_to_hours = 24
name_of_unit = "hours"

def days_to_units(num_of_days, custom_message):
    print(f"{num_of_days} days are {num_of_days * calculation_to_hours} {name_of_unit}")
    print(custom_message)

scope_check()

# Accepting_user_input:

def days_to_units(num_of_days):
    print(f"{num_of_days} days are {num_of_days * calculation_to_units} {name_of_unit}")

my_var = days_to_units(20)

user_input = input("Hey user, enter a number of a days and I will convert it to hours!\n")
print(user_input)

#Conditionals and Boolean Data Types
calculation_to_units = 24
name_of_unit = "hours"

def days_to_units(num_of_days):
    if num_of_days > 0:
        return f"{num_of_days} days are {num_of_days * calculation_to_units} {name_of_unit}"
    elif num_of_days == 0:
        return "you entered a 0, please enter a valie positive number"
    
def validate_and_execute():
    if user_input.isdigit():
        user_input_number = int(user_input)
        calculated_value = days_to_units(user_input_number)
        print(calculated_value)
    else:
        print("your input is not a valid number.Don`t ruin my program!")

# try adding a catch block using try block with exceptions of valueErrors
        # try: 
        #     user_input_number = int(user_input)
        # except ValueError:
        #     print("your input is not a valid number. Don`t ruin my program !!")

#while loops
while user_input != "exit":
    user_input = input("Hey user, enter a number of days as a comma separated list and I will convert it to hours!\n")
    print(type(user_input.split(",")))
    print(user_input.split(","))
    for num_of_days_element in user_input.split(","):
        validate_and_execute()

print("some text")
input("enter value")
set([1, 3, 4])
int("20")
"2, 3".split()



user_input = ""
while user_input != "exit":
    user_input = input("Hey user, enter number of days and conversion unit !\n")
    days_and_unit = user_input.split(":")
    print(days_and_unit)
    days_and_unit_dictionary = {"days": days_and_unit[0], "unit":days_and_unit[1]}
    print(days_and_unit)
    days_and_unit_dictionary = {"days": days_and_unit[0], "unit": days_and_unit[1]}
    print(type(days_and_unit_dictionary))
    validate_and_execute()

    
{"days":20, "unit":"hours"}
my_list = ["20", "30", "100"]
print(my_list[2])

my_dictionary = {"days": 20, "unit":"hours"}

message = "enter some value"
days = 20
price = 9.99
valid_number = True
exit_input = False
list_of_days = [20, 40, 20, 100]
list_of_months = ["January"]

from helper import validate_and_execute, user_input_message
import logging

logger = logging.getLoggger("MAIN")
logger.error("Error happened in the app")


